﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TechHut.DataAccess.Repository.IRepository;
using TechHut.Models;
using TechHut.Models.ViewModels;

namespace TechHut.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ClientReviewController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ClientReviewController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            this._unitOfWork = unitOfWork;
            this._hostEnvironment = hostEnvironment;
        }


        public IActionResult Add()
        {
           return View();
        }

        public IActionResult Edit(int? Id)
        {
            var ClientReview = _unitOfWork.ClientReview.GetFirstOrDefault(f => f.Id == Id);
            var clientReviewVm = new VmClientReview()
            {
                Id = ClientReview.Id,
                Designation = ClientReview.Designation,
                Name = ClientReview.Name,
                Review = ClientReview.Review,
                ImageUrl = ClientReview.ImageUrl,
               
            };
            return View(clientReviewVm);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult Create(VmClientReview model)
        {
            if (ModelState.IsValid)
            {
                var imgUrl = "";
                string wwRootPath = _hostEnvironment.WebRootPath;
                if (model.Image == null)
                {
                }
                else
                {
                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(wwRootPath, @"Images\Clients");
                    var extention = Path.GetExtension(model.Image.FileName);
                    using (var fileSteams = new FileStream(Path.Combine(uploads, fileName + extention), FileMode.Create))
                    {
                        model.Image.CopyTo(fileSteams);

                    }
                    imgUrl = @"\images\Clients\" + fileName + extention;
                }
                try
                {
                    var clientReview = new ClientReview()
                    {
                        Id = model.Id,
                        Designation = model.Designation,
                        ImageUrl = imgUrl,
                        Name = model.Name,
                        Review = model.Review,

                    };
                     _unitOfWork.ClientReview.Add(clientReview);
                    _unitOfWork.Save();
                }
                catch(Exception ex)
                {
                    var message = ex.Message;
                }
            }
            return Redirect("/Admin/ClientReview/Index");
        }
        [HttpPost]
        public IActionResult Update(VmClientReview model)
        {
            if (ModelState.IsValid)
            {
                var imgUrl = "";
                string wwRootPath = _hostEnvironment.WebRootPath;
                if (model.Image != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(wwRootPath, @"Images\Clients");
                    var extention = Path.GetExtension(model.Image.FileName);

                    if (model.ImageUrl != null)
                    {
                        var oldImgPath = Path.Combine(wwRootPath, model.ImageUrl.TrimStart('\\'));
                        if (System.IO.File.Exists(oldImgPath))
                        {
                            System.IO.File.Delete(oldImgPath);
                        }
                    }
                    using (var fileSteams = new FileStream(Path.Combine(uploads, fileName + extention), FileMode.Create))
                    {
                        model.Image.CopyTo(fileSteams);
                    }
                    imgUrl = @"\images\Clients\" + fileName + extention;
                }
                else
                {
                    imgUrl = model.ImageUrl;
                }
                var clientReview = _unitOfWork.ClientReview.GetFirstOrDefault(f => f.Id == model.Id);
                clientReview.Name = model.Name;
                clientReview.Review = model.Review;
                clientReview.Designation = model.Designation;
                clientReview.ImageUrl = imgUrl;
                _unitOfWork.ClientReview.UpdateClientReview(clientReview);
                _unitOfWork.Save();
            }

            return Redirect("/Admin/ClientReview/Index");
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ClientReviewList = _unitOfWork.ClientReview.GetAll();

            return Json(new { data = ClientReviewList });
        }


        [HttpDelete]
        public IActionResult Delete(int? id)
        {


            var ClientReview = _unitOfWork.ClientReview.GetFirstOrDefault(f => f.Id == id);
            if (ClientReview == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }

            _unitOfWork.ClientReview.Remove(ClientReview);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted Successfully!" });
        }

    }
}
