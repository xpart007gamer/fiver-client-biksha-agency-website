﻿using TechHut.DataAccess.Repository.IRepository;
using TechHut.Models;
using TechHut.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace TechHut.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ProductController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            this._unitOfWork = unitOfWork;
            this._hostEnvironment = hostEnvironment;
        }

        public IActionResult Index()
        {
            //var products = _unitOfWork.Product.GetAll();
            return View();
        }

        public IActionResult Upsert(int? id) 
        {
            ProductVM productVM = new()
            {
                Product = new(),
                CategoryList = _unitOfWork.Category.GetAll().Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }),
                CoverTypeList = _unitOfWork.CoverType.GetAll().Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() })
            };
            

            if (id == null || id == 0)
            {
                
                return View(productVM);
            }
            else
            {
                productVM.Product = _unitOfWork.Product.GetFirstOrDefault(f => f.Id == id);
                return View(productVM);
            }
          
          
         
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(ProductVM model)
        {
            
            if (ModelState.IsValid)
            {
                string wwRootPath = _hostEnvironment.WebRootPath;
                if (model.Image != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(wwRootPath, @"Images\Products");
                    var extention = Path.GetExtension(model.Image.FileName);

                    if(model.Product.ImageUrl != null)
                    {
                        var oldImgPath = Path.Combine(wwRootPath, model.Product.ImageUrl.TrimStart('\\'));
                        if (System.IO.File.Exists(oldImgPath))
                        {
                            System.IO.File.Delete(oldImgPath);
                        }
                    }
                    using (var fileSteams = new FileStream(Path.Combine(uploads, fileName + extention), FileMode.Create))
                    {
                        model.Image.CopyTo(fileSteams);

                    }
                    model.Product.ImageUrl = @"\images\products\" + fileName + extention;
                }
               
                try
                {
                    if(model.Product.Id == 0)
                    {
                        _unitOfWork.Product.Add(model.Product);
                    }
                    else
                    {
                        _unitOfWork.Product.Update(model.Product);
                    }
                 
                    _unitOfWork.Save();
                    TempData["success"] = "Product Created Succesfully!";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                  var err = ex.Message;
                }
             
            }

            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var Product = _unitOfWork.Product.GetFirstOrDefault(f => f.Id == id);
            if (Product == null)
            {
                return NotFound();
            }
            return View(Product);
        }

        #region API Calls
        [HttpGet]
        public IActionResult GetAll()
        {
            var productList = _unitOfWork.Product.GetAll(includeProperties:"Category,CoverType");

            return Json(new { data = productList });
        }

        [HttpDelete]
        public IActionResult Delete(int? id)
        {

            var product = _unitOfWork.Product.GetFirstOrDefault(f => f.Id == id);
            if (product == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }
            var oldImgPath = Path.Combine(_hostEnvironment.WebRootPath, product.ImageUrl.TrimStart('\\'));
            if (System.IO.File.Exists(oldImgPath))
            {
                System.IO.File.Delete(oldImgPath);
            }
            _unitOfWork.Product.Remove(product);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted Successfully!" });
        }
        #endregion
    }


}
