﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TechHut.DataAccess;

namespace TechHut.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles= "Admin")]
    public class DashboardController : Controller
    {
      
        public IActionResult Index()
        {

            return View();
        }
    }
}
