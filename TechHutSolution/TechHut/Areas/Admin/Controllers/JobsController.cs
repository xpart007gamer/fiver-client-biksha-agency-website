﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TechHut.DataAccess.Repository.IRepository;
using TechHut.Models;
using TechHut.Models.ViewModels;

namespace TechHut.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class JobsController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public JobsController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            this._unitOfWork = unitOfWork;
            this._hostEnvironment = hostEnvironment;
        }
        public IActionResult Add()
        {
            return View();
        }
        public IActionResult Edit(int? Id)
        {
            var Jobs = _unitOfWork.Jobs.GetFirstOrDefault(f => f.Id == Id);
            var jobsVm = new VmJobs()
            {
                Id = Jobs.Id,
                Description = Jobs.Description,
                Name = Jobs.Name,
                Location = Jobs.Location,
                Responsibilities = Jobs.Responsibilities,
                ExperienceRequirement = Jobs.ExperienceRequirement,
                EqualOpportunityEmployer = Jobs.EqualOpportunityEmployer,
                IsFeatured = Jobs.IsFeatured,
                ExperienceLevel = Jobs.ExperienceLevel,
                Role = Jobs.Role,   
                JobType = Jobs.JobType, 
            
    

            };
            return View(jobsVm);
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(VmJobs model)
        {
            if (ModelState.IsValid)
            {
               
                try
                {
                    var jobs = new Jobs()
                    {
                        Id = model.Id,
                        Description = model.Description != null ? model.Description : "",
                        Name = model.Name != null ? model.Name : "",
                        Location = model.Location != null ? model.Location : "",
                        Responsibilities = model.Responsibilities != null ? model.Responsibilities : "",
                        ExperienceRequirement = model.ExperienceRequirement != null ? model.ExperienceRequirement : "",
                        EqualOpportunityEmployer = model.EqualOpportunityEmployer != null ? model.EqualOpportunityEmployer : "",
                        ExperienceLevel = model.ExperienceLevel != null ? model.ExperienceLevel : "",
                        IsFeatured = model.IsFeatured,
                        JobType = model.JobType != null ? model.JobType : "",
                        LatLong = model.LatLong != null ? model.LatLong : "",
                        Role = model.Role != null ? model.Role : ""

                    };
                    _unitOfWork.Jobs.Add(jobs);
                    _unitOfWork.Save();
                }
                catch (Exception ex)
                {
                    var message = ex.Message;
                }
            }
            return Redirect("/Admin/Jobs/Index");
        }
        [HttpPost]
        public IActionResult Update(VmJobs model)
        {
            if (ModelState.IsValid)
            {
               
                var Jobs = _unitOfWork.Jobs.GetFirstOrDefault(f => f.Id == model.Id);
                Jobs.Name = model.Name != null ? model.Name : "";
                Jobs.Description = model.Description != null ? model.Description : "";
                Jobs.Location = model.Location != null ? model.Location : "";
                Jobs.Responsibilities = model.Responsibilities != null ? model.Responsibilities : "";
                Jobs.ExperienceRequirement = model.ExperienceRequirement != null ? model.ExperienceRequirement : "";
                Jobs.EqualOpportunityEmployer = model.EqualOpportunityEmployer != null ? model.EqualOpportunityEmployer : "";
                Jobs.JobType = model.JobType != null ? model.JobType : "";
                Jobs.Role = model.Role != null ? model.Role : "";
                Jobs.ExperienceLevel = model.ExperienceLevel != null ? model.ExperienceLevel : "";
                Jobs.IsFeatured = model.IsFeatured;
                _unitOfWork.Jobs.UpdateJobs(Jobs);
                _unitOfWork.Save();
            }

            return Redirect("/Admin/Jobs/Index");
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var JobsList = _unitOfWork.Jobs.GetAll();

            return Json(new { data = JobsList });
        }


        [HttpDelete]
        public IActionResult Delete(int? id)
        {


            var Jobs = _unitOfWork.Jobs.GetFirstOrDefault(f => f.Id == id);
            if (Jobs == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }

            _unitOfWork.Jobs.Remove(Jobs);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted Successfully!" });
        }
    }
}
