﻿using Microsoft.AspNetCore.Mvc;
using TechHut.DataAccess;
using TechHut.DataAccess.Repository.IRepository;

namespace TechHut.Web.Controllers
{

    public class HomeController : Controller
    {
        private readonly IClientReviewRepository _clientReviewRepository;
        public HomeController(IClientReviewRepository clientReviewRepository)
        {
            _clientReviewRepository = clientReviewRepository;
        }
        public ActionResult Index()
        {
            ViewBag.ClientReviews = _clientReviewRepository.GetAll();
            return View();  
        }
    
    }
}
