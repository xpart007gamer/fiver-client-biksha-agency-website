﻿using Microsoft.AspNetCore.Mvc;
using TechHut.DataAccess.Repository.IRepository;
using TechHut.Models.ViewModels;

namespace TechHut.Web.Controllers
{
    public class CareersController : Controller
    {
        private readonly IJobsRepository _jobsRepository;
        public CareersController(IJobsRepository jobsRepository)
        {
            _jobsRepository = jobsRepository;
        }
        public IActionResult Index()
        {
            var jobList = _jobsRepository.GetAll();
            ViewBag.JobList = jobList;
            return View(jobList);
        }

        public IActionResult JobDetails(int Id)
        {
            var jobDetails = _jobsRepository.GetFirstOrDefault(f => f.Id == Id);
            var jobList = _jobsRepository.GetAll();
            var Vmjob = new VmJobDetails();
            Vmjob.jobList = jobList;
            Vmjob.Jobs = jobDetails;

            return View(Vmjob);
        }
       
    }
}
