﻿using Microsoft.AspNetCore.Mvc;
using TechHut.DataAccess;
using TechHut.Models.ViewModels;
using TechHut.Utility.Services;

namespace TechHut.Web.Controllers
{
    public class ContactUsController : Controller
    {
        private readonly IEmailService _emailService;
        public ContactUsController(IEmailService emailService)
        {
            _emailService = emailService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SendEmail(VmSendEmail model)
        {

            _emailService.SendEmail(model.Name,model.Email,model.Message);
            return View();
        }
    }
}
