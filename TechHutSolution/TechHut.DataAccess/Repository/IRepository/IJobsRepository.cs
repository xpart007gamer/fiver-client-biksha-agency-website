﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechHut.Models;

namespace TechHut.DataAccess.Repository.IRepository
{
    public interface IJobsRepository : IRepository<Jobs>
    {
        void UpdateJobs(Jobs jobs);


    }
}
