﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechHut.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork
    {
        ICategoryRepository Category { get; }
        IClientReviewRepository ClientReview { get; }
        IJobsRepository Jobs { get; }


        void Save();
    }
}
