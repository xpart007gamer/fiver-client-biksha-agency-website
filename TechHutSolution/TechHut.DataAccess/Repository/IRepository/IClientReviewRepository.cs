﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechHut.Models;

namespace TechHut.DataAccess.Repository.IRepository
{
    public interface IClientReviewRepository : IRepository<ClientReview>
    {
        void UpdateClientReview(ClientReview clientReview);


    }
}
