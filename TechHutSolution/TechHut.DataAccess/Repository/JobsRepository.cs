﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechHut.DataAccess.Repository.IRepository;
using TechHut.Models;

namespace TechHut.DataAccess.Repository
{
    public class JobsRepository : Repository<Jobs>, IJobsRepository
    {
        private ApplicationDbContext _db;

        public JobsRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void UpdateJobs(Jobs jobs)
        {
            _db.Jobs.Update(jobs);
        }
    }
}
