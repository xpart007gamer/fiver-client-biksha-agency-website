﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechHut.DataAccess.Repository.IRepository;
using TechHut.Models;

namespace TechHut.DataAccess.Repository
{
    public class ClientReviewRepository : Repository<ClientReview>, IClientReviewRepository
    {
        private ApplicationDbContext _db;

        public ClientReviewRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void UpdateClientReview(ClientReview clientReview)
        {
            _db.ClientReview.Update(clientReview);
        }
    }
}
