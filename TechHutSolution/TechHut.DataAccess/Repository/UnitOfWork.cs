﻿using TechHut.DataAccess.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechHut.Models;

namespace TechHut.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _db;

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            Category = new CategoryRepository(_db);
            ClientReview = new ClientReviewRepository(_db);
            Jobs = new JobsRepository(_db);

        }
        public ICategoryRepository Category { get; private set; }
        public IClientReviewRepository ClientReview { get; private set; }
        public IJobsRepository Jobs { get; private set; }


        public void Save()
        {
            _db.SaveChanges();
        }

       
    }
}
