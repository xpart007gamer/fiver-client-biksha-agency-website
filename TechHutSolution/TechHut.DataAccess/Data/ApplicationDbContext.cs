﻿
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TechHut.Models;

namespace TechHut.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) 
        {
             
        }

        public DbSet<Category> Categories { get; set; }
     
  
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ClientReview> ClientReview { get; set; }
        public DbSet<Jobs> Jobs { get; set; }


    }
}
