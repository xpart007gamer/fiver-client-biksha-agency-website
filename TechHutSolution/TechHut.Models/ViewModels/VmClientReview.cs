﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechHut.Models.ViewModels
{
    public class VmClientReview
    {
        public int Id { get; set; }
        public IFormFile Image { get; set; }

        [Required(ErrorMessage = "Review is Required")]
        public string Review { get; set; }
        
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }//required

        [Required(ErrorMessage = "Designation is Required")]
        public string Designation { get; set; }//required

        public string? ImageUrl { get; set; }//required
    }
}
