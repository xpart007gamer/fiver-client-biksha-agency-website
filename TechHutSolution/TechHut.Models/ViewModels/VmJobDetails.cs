﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechHut.Models.ViewModels
{
    public class VmJobDetails
    {
        public IEnumerable<Jobs> jobList { get; set; }
        public Jobs Jobs { get; set; }
    }
}
