﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechHut.Models
{
    public class ClientReview
    {
        [Key]
        public int Id { get; set; }
        public string Review { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }

        [ValidateNever]
        public string? ImageUrl { get; set; }
    }
}
