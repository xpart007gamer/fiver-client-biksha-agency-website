﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TechHut.Models.Enums.Enums;

namespace TechHut.Models
{
    public class Jobs
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LatLong { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Responsibilities { get; set; }
        public string ExperienceRequirement { get; set; }
        public string EqualOpportunityEmployer { get; set; }
        public string JobType { get; set; }
        public string ExperienceLevel { get; set; }
        public string Role { get; set; }

        public bool IsFeatured { get; set; }

    }
}
