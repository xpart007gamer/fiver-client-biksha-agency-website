﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechHut.Utility.Services
{
    public interface IEmailService
    {
        bool SendEmail(string Name, string Email, string Message);
        bool SendEmailPasswordReset(string Name, string Email, string Message);
    }
}
